#include <WiFi.h> 
#include <Arduino.h>
#include "DHT.h"  
#include <PubSubClient.h>
#include <ArduinoJson.h> // Include ArduinoJson Library
#include <MQ135.h>

const char* WIFI_SSID = "Duy_2,4G";
const char* WIFI_PASSWORD = "Mothai*#ba4.";
// const char* WIFI_SSID = "minhchi1509";
// const char* WIFI_PASSWORD = "ptit_15092002";
const char* MQTT_HOST = "broker.hivemq.com";
const int   MQTT_PORT = 1883;
const char* MQTT_USER = "minhchi1509";
const char* MQTT_PASSWORD = "ptit_15092002";
const char* MQTT_CLIENT_ID = "minhchiiotptit";
const char* MQTT_TOPIC_METRICS = "esp32/metrics";
const char* MQTT_TOPIC_FAN = "esp32/fan";
const char* MQTT_TOPIC_CONTROL_FAN = "esp32/controlfan";

#define MQ135_THRESHOLD 45// Fresh Air threshold
const int DHTTYPE = DHT11;  
const int DHTPIN = 26;  
const int MQPIN1 = 33;
const int MQPIN2 = 35;
const int BUZZERPIN = 32;
const int BUZZERPIN2 = 16;
const int RELAYPIN = 2;

DHT dht(DHTPIN, DHTTYPE);
MQ135 mq135_sensor_1 (MQPIN1);
MQ135 mq135_sensor_2 (MQPIN2);
WiFiClient wifiClient;
void callback(char *topic, byte *payload, unsigned int length);
PubSubClient mqttClient(MQTT_HOST, MQTT_PORT, callback, wifiClient);

#define NUM_LOCATIONS 2

float temp[NUM_LOCATIONS];
float humid[NUM_LOCATIONS];
float air[NUM_LOCATIONS];
boolean priority1 = false;
boolean priority2 = false;

void connectWiFi() {
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nWiFi connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


void mqttConnect()
{
  while (!mqttClient.connected())
  {
    Serial.println("Attempting MQTT connection...");
    if (mqttClient.connect(MQTT_CLIENT_ID))
    {
      Serial.println("MQTT Client Connected");
      mqttClient.subscribe(MQTT_TOPIC_CONTROL_FAN);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void publishMetric() {
  StaticJsonDocument<200> doc;
  JsonArray metricsArray = doc.to<JsonArray>();

  // Duyệt qua từng vị trí và thêm thông tin vào mảng
  for (int i = 0; i < NUM_LOCATIONS; i++) {
    JsonObject metric = metricsArray.createNestedObject();
    metric["location"] = "Location " + String(i + 1);
    metric["temperature"] = temp[i];
    metric["humidity"] = humid[i];
    metric["airQualityIndex"] = air[i];
  }

  // Serialize mảng thành chuỗi JSON
  String jsonString;
  serializeJson(doc, jsonString);

  // Publish chuỗi JSON lên MQTT
  mqttClient.publish(MQTT_TOPIC_METRICS, (char *)jsonString.c_str());
}

// void handleToggleFan2(String status){
//   if (status.equals("OFF")){
//     digitalWrite(RELAYPIN, HIGH);
//   }else{
//     digitalWrite(RELAYPIN, LOW);
//   }
// }

void handleToggleFan1(String status){
  if (status.equals("ON")){
    digitalWrite(RELAYPIN, LOW);
  }else{
    digitalWrite(RELAYPIN, HIGH);
  }
}

void handleToggleWhistle1(String status){
  if (status.equals("ON")){
    digitalWrite(BUZZERPIN, HIGH);
  }
  else{
    digitalWrite(BUZZERPIN, LOW);
  }
}
void handleToggleWhistle2(String status){
  if (status.equals("ON")){
    digitalWrite(BUZZERPIN2, HIGH);
  }
  else{
    digitalWrite(BUZZERPIN2, LOW);
  }
}
String deviceStatus[2];
void handleAirPollution(){
  // isPollution1 = air[0] > MQ135_THRESHOLD ? true : false;
  // deviceStatus[0] = isPollution1 ? "ON" : "OFF";
  // isPollution2 = air[1] > MQ135_THRESHOLD ? true : false;
  // deviceStatus[1] = isPollution2 ? "ON" : "OFF";
  // // handleToggleFan1(deviceStatus[0]);
  // handleToggleWhistle1(deviceStatus[0]);
  // handleToggleWhistle2(deviceStatus[1]);
  if(priority1 == false){
    boolean isPollution1 = air[0] > MQ135_THRESHOLD ? true : false;
    deviceStatus[0] = isPollution1 ? "ON" : "OFF";
    handleToggleWhistle1(deviceStatus[0]);
    handleToggleFan1(deviceStatus[0]);
  }
  if(priority2 == false){
    boolean isPollution2 = air[1] > MQ135_THRESHOLD ? true : false;
    deviceStatus[1] = isPollution2 ? "ON" : "OFF";
    handleToggleWhistle2(deviceStatus[1]);
  }
  String status;
  StaticJsonDocument<200> doc;
  JsonArray fansArray = doc.to<JsonArray>();

  //Duyệt qua từng vị trí và thêm thông tin vào mảng
  for (int i = 0; i < NUM_LOCATIONS; i++) {
    JsonObject fan = fansArray.createNestedObject();
    fan["location"] = "Location " + String(i + 1);
    fan["deviceStatus"] = deviceStatus[i];
  }
  String jsonString;
  serializeJson(doc, jsonString);
  mqttClient.publish(MQTT_TOPIC_FAN, (char *)jsonString.c_str());
}

void setup() {
  Serial.begin(115200);
  dht.begin();  
  connectWiFi();
  mqttConnect();
  pinMode(BUZZERPIN, OUTPUT);
  pinMode(BUZZERPIN2, OUTPUT);
  pinMode(RELAYPIN, OUTPUT);
}

void assignValue() {
  temp[0] = dht.readTemperature();
  humid[0] = dht.readHumidity();
  air[0] = map(analogRead(MQPIN1), 0, 1024, 0, 100);

  temp[1] = temp[0]-1;
  humid[1] = humid[0]-3;
  air[1] = map(analogRead(MQPIN2), 0, 1024, 0, 100);
}

void loop() {
  assignValue();
  // Serial.print("Temp: ");
  // Serial.print(temp[0]);
  // Serial.print(" Humid: ");
  // Serial.print(humid[0]);
  // Serial.print(" Air: ");
  // Serial.print(air[0]);
  // Serial.print(" Temp: ");
  // Serial.print(temp[1]);
  // Serial.print(" Humid: ");
  // Serial.print(humid[1]);
  // Serial.print(" Air: ");
  // Serial.print(air[1]);
  publishMetric();
  handleAirPollution();
  Serial.println();
  if (!mqttClient.loop())
    mqttConnect();
  delay(10000);
}

void callback(char *topic, byte *payload, unsigned int length)
{
  String command;
  String s1;
  String s2;
  int i;
  int check = 0;
  for (i = 1; i < length; i++) {
    command += (char)payload[i];
  }
  Serial.println(command);
  s1 = command[command.length()-4];
  s1 += command[command.length()-3];
  int location;
  for( i=1 ; i<command.length();i++){
    if(command[i]=='1') {
      location=1;
    }if(command[i]=='2') {
      location=2;
    }
  }
  String receivedTopic = String(topic);
  if (receivedTopic == MQTT_TOPIC_CONTROL_FAN) {
    if (s1.equals("ON")){
      if(location==1){
        deviceStatus[0]="ON";
        priority1 = true;
        handleToggleFan1("ON");
      }
      else{
        deviceStatus[1]="ON";
        priority2 = true;
      }
    }
    else{
      if(location==1){
        deviceStatus[0]="OFF";
        priority1 = true;
        handleToggleFan1("OFF");
      }
      else{
        deviceStatus[1]="OFF";
        priority2 = true;
      }
    }
  }
}

