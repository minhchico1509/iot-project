export type IMetrics = {
  location: string;
  temperature: number;
  humidity: number;
  airQualityIndex: number;
  airQuality: string;
  createdAt: string;
};
