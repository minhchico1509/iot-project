interface ColumnType<T extends object> {
  title: string;
  dataIndex: keyof T;
  render?: (record: T) => React.ReactNode;
}

export type ColumnsType<T extends object> = ColumnType<T>[];

export type TRowData = {
  rowIndex: number;
  location: string;
  temperature: number;
  humidity: number;
  airQualityIndex: number;
  airQuality: string;
  createdAt: string;
};
