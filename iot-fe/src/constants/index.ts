export const APP_BAR_LIST = [
  {
    label: "CONTROL",
    path: "/control",
  },
  {
    label: "HISTORY",
    path: "/metric-history",
  },
];

export const POLLUTION_THRESHOLD = 45;
export const AIR_QUALITY_OPTIONS = [
  {
    label: "Trong sạch (Fresh)",
    value: "FRESH",
  },
  {
    label: "Ô nhiễm (Pollution)",
    value: "POLLUTION",
  },
];
