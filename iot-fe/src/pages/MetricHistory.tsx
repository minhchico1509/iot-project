import { Box, Chip, Typography } from "@mui/material";
import axios from "axios";
import dayjs from "dayjs";
import { pickBy } from "lodash";
import { useState } from "react";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { GreenButton } from "src/components/Button";
import DataTable from "src/components/DataTable";
import DateTimePicker from "src/components/DateTimePicker";
import Dropdown from "src/components/Dropdown";
import { AIR_QUALITY_OPTIONS } from "src/constants";
import { ColumnsType, TRowData } from "src/types/table.types";
import { formatTableRowData } from "src/utils/common.utils";

interface IForm {
  startDateTime: string;
  endDateTime: string;
  location: string;
  quality: string;
}

const columns: ColumnsType<TRowData> = [
  {
    dataIndex: "rowIndex",
    title: "STT",
  },
  {
    dataIndex: "location",
    title: "Địa điểm",
  },
  {
    dataIndex: "temperature",
    title: "Nhiệt độ",
    render: (record) => <Typography>{`${record.temperature}°C`}</Typography>,
  },
  {
    dataIndex: "humidity",
    title: "Độ ẩm",
    render: (record) => <Typography>{`${record.humidity}%`}</Typography>,
  },
  {
    dataIndex: "airQualityIndex",
    title: "Chỉ số chất lượng không khí",
    render: (record) => <Typography>{`${record.airQualityIndex}`}</Typography>,
  },
  {
    dataIndex: "airQuality",
    title: "Đánh giá chất lượng không khí",
    render: (record) => (
      <Chip
        clickable
        variant="filled"
        label={record.airQuality}
        color={record.airQuality === "Fresh" ? "success" : "error"}
      />
    ),
  },
  {
    dataIndex: "createdAt",
    title: "Thời gian",
  },
];

const MetricHistory = () => {
  const [metricHistoryData, setMetricHistoryData] = useState<TRowData[]>([]);

  const { control, handleSubmit } = useForm<IForm>({
    shouldFocusError: false,
    defaultValues: {
      startDateTime: "",
      endDateTime: "",
      quality: "",
      location: "",
    },
  });

  const onSubmitForm: SubmitHandler<IForm> = async (data) => {
    const sanitizedData = pickBy(data, (value) => value.length > 0);
    console.log(sanitizedData);
    const response = await axios.get("http://localhost:8080/metrics", {
      params: { ...sanitizedData },
    });
    console.log(formatTableRowData(response.data));

    setMetricHistoryData(formatTableRowData(response.data));
  };

  return (
    <>
      <Box display="flex" gap={3} alignItems="center">
        <Controller
          control={control}
          name="startDateTime"
          render={({ field, fieldState: { error } }) => (
            <DateTimePicker
              label="Thời gian bắt đầu"
              format="DD/MM/YYYY HH:mm:ss"
              value={dayjs(field.value)}
              onChange={(value) => field.onChange(value?.toISOString())}
              errorMessage={error?.message}
              timeSteps={{ hours: 1, minutes: 1, seconds: 1 }}
            />
          )}
        />
        <Controller
          control={control}
          name="endDateTime"
          render={({ field, fieldState: { error } }) => (
            <DateTimePicker
              label="Thời gian kết thúc"
              format="DD/MM/YYYY HH:mm:ss"
              value={dayjs(field.value)}
              onChange={(value) => field.onChange(value?.toISOString())}
              errorMessage={error?.message}
              timeSteps={{ hours: 1, minutes: 1, seconds: 1 }}
            />
          )}
        />
        <Controller
          control={control}
          name="location"
          render={({
            field: { ref, ...otherFields },
            fieldState: { error },
          }) => (
            <Dropdown
              label="Địa điểm"
              options={[
                { label: "Location 1", value: "Location 1" },
                { label: "Location 2", value: "Location 2" },
              ]}
              {...otherFields}
              error={!!error?.message}
              helperText={error?.message}
              sx={{ width: "250px" }}
            />
          )}
        />
        <Controller
          control={control}
          name="quality"
          render={({
            field: { ref, ...otherFields },
            fieldState: { error },
          }) => (
            <Dropdown
              label="Chất lượng không khí"
              options={AIR_QUALITY_OPTIONS}
              {...otherFields}
              error={!!error?.message}
              helperText={error?.message}
              sx={{ width: "250px" }}
            />
          )}
        />
        <GreenButton onClick={handleSubmit(onSubmitForm)}>Lọc</GreenButton>
      </Box>
      <DataTable columns={columns} dataSource={metricHistoryData} />
    </>
  );
};

export default MetricHistory;
