import { Box, Paper, Typography, Chip } from "@mui/material";
import { useEffect, useState } from "react";
import {
  AirQualityIcon,
  FanIcon,
  HumidityIcon,
  TemperatureIcon,
} from "src/assets/icons";
import { GreenButton, PurpleButton } from "src/components/Button";
import Dropdown from "src/components/Dropdown";
import { Loading } from "src/components/Loading";
import MetricCardInfo from "src/components/MetricCardInfo";
import { useSocket } from "src/hooks";
import { IMetrics } from "src/types/common.types";

const Control = () => {
  const { socket } = useSocket();
  const [allMetrics, setAllMetrics] = useState<IMetrics[]>();
  const [locationMetric, setLocationMetric] = useState<IMetrics>();
  const [selectedLocation, setSelectedLocation] =
    useState<string>("Location 1");

  const [allFanStatus, setAllFanStatus] =
    useState<{ location: string; deviceStatus: string }[]>();
  const [locationFan, setLocationFan] = useState<{
    location: string;
    deviceStatus: string;
  }>();

  useEffect(() => {
    socket?.on("esp32/metrics", (data) => {
      setAllMetrics(data);
    });

    socket?.on("esp32/fan", (data) => {
      setAllFanStatus(data);
    });

    return () => {
      socket?.off("esp32/metrics");
      socket?.off("fan");
    };
  }, [socket]);

  useEffect(() => {
    if (!locationMetric) {
      setLocationMetric(allMetrics?.[0]);
    }
    if (!locationFan) {
      setLocationFan(allFanStatus?.[0]);
    }
  }, [allMetrics, allFanStatus]);

  useEffect(() => {
    const newLocationMetric = allMetrics?.find(
      (e) => e.location === selectedLocation
    );
    const newLocationFan = allFanStatus?.find(
      (e) => e.location === selectedLocation
    );
    setLocationMetric(newLocationMetric);
    setLocationFan(newLocationFan);
  }, [selectedLocation]);

  return (
    <>
      {locationMetric ? (
        <>
          <Dropdown
            options={[
              { label: "Location 1", value: "Location 1" },
              { label: "Location 2", value: "Location 2" },
            ]}
            value={selectedLocation}
            onChange={(e) => setSelectedLocation(e.target.value)}
            sx={{
              minWidth: 300,
              mb: 2,
            }}
            label="Chọn địa điểm"
          />
          <Typography variant="h4" fontWeight={700} mb={2}>
            {`Địa điểm: ${locationMetric.location}`}
          </Typography>
          <Box display="flex" justifyContent="space-evenly">
            <MetricCardInfo
              icon={<TemperatureIcon width={90} height={90} />}
              title="Nhiệt độ"
              measurement={`${locationMetric?.temperature.toFixed(1)}°C`}
            />
            <MetricCardInfo
              icon={<HumidityIcon width={90} height={90} />}
              title="Độ ẩm"
              measurement={`${locationMetric?.humidity.toFixed(1)}%`}
            />
            <MetricCardInfo
              icon={<AirQualityIcon width={90} height={90} />}
              title="Chất lượng không khí"
              measurement={`${locationMetric?.airQualityIndex.toFixed(1)}`}
              type="air"
            />
          </Box>
          <Box display="flex" justifyContent="center" mt={5}>
            <Paper
              elevation={3}
              sx={{
                height: 250,
                width: 400,
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Box py={1} textAlign="center" borderBottom="1px solid #bdbdbd">
                <Typography variant="h4" fontWeight="bold">
                  Điều khiển quạt
                </Typography>
              </Box>
              <Box
                display="flex"
                alignItems="center"
                gap={6}
                p={1}
                flex={1}
                justifyContent="space-evenly"
              >
                <FanIcon width={90} height={90} />
                <Box display="flex" flexDirection="column" alignItems="center">
                  <Box display="flex" gap={2} alignItems="center">
                    <Typography fontWeight="bold">Trạng thái:</Typography>
                    <Chip
                      label={locationFan?.deviceStatus}
                      variant="filled"
                      color={
                        locationFan?.deviceStatus === "ON" ? "success" : "error"
                      }
                    />
                  </Box>
                  <Box display="flex" gap={2} alignItems="center" mt={2}>
                    <GreenButton
                      onClick={() =>
                        socket?.emit("toggle-fan", {
                          location: selectedLocation,
                          status: "ON",
                        })
                      }
                    >
                      Bật quạt
                    </GreenButton>
                    <PurpleButton
                      onClick={() =>
                        socket?.emit("toggle-fan", {
                          location: selectedLocation,
                          status: "OFF",
                        })
                      }
                    >
                      Tắt quạt
                    </PurpleButton>
                  </Box>
                </Box>
              </Box>
            </Paper>
          </Box>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Control;
