import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import CommonLayout from "src/layouts/CommonLayout";
import Control from "src/pages/Control";
import MetricHistory from "src/pages/MetricHistory";

const AppRoutes = () => {
  return (
    <Routes>
      <Route element={<CommonLayout />}>
        <Route path="/control" element={<Control />} />
        <Route path="/metric-history" element={<MetricHistory />} />
      </Route>
      <Route path="*" element={<Navigate to="/control" />} />
    </Routes>
  );
};

export default AppRoutes;
