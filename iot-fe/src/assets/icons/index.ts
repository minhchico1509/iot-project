import TemperatureIcon from "./temperature.svg?react";
import HumidityIcon from "./humidity.svg?react";
import AirQualityIcon from "./air-quality.svg?react";
import FanIcon from "./fan.svg?react";

export { TemperatureIcon, HumidityIcon, AirQualityIcon, FanIcon };
