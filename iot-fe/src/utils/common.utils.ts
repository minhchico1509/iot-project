import dayjs from "dayjs";
import { TRowData } from "src/types/table.types";

export const formatTableRowData = (originalData: any): TRowData[] => {
  const result: TRowData[] = originalData.map((data: any, index: number) => ({
    rowIndex: index + 1,
    location: data.location,
    temperature: +data.temperature,
    humidity: +data.humidity,
    airQualityIndex: +data.airQualityIndex,
    airQuality: `${data.airQuality.charAt(0).toUpperCase()}${data.airQuality
      .slice(1)
      .toLowerCase()}`,
    createdAt: dayjs(data.createdAt).format("DD/MM/YYYY HH:mm:ss"),
  })) as TRowData[];
  return result;
};
