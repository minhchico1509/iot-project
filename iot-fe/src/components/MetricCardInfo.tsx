import { Paper, Box, Typography, Chip } from "@mui/material";
import { POLLUTION_THRESHOLD } from "src/constants";

interface IMetricCardInfoProps {
  title: string;
  icon: React.ReactNode;
  measurement: string;
  type?: string;
}

const MetricCardInfo: React.FC<IMetricCardInfoProps> = ({
  title,
  icon,
  measurement,
  type,
}) => {
  return (
    <Paper
      elevation={3}
      sx={{
        height: 250,
        width: 400,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box py={1} textAlign="center" borderBottom="1px solid #bdbdbd">
        <Typography variant="h4" fontWeight="bold">
          {title}
        </Typography>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        gap={6}
        p={1}
        flex={1}
        justifyContent="space-evenly"
      >
        {icon}
        <Box display="flex" flexDirection="column" alignItems="center">
          {type === "air" && (
            <Chip
              label={+measurement > POLLUTION_THRESHOLD ? "Pollution" : "Fresh"}
              variant="filled"
              color={+measurement > POLLUTION_THRESHOLD ? "error" : "success"}
            />
          )}
          <Typography variant="h3" fontWeight="bold">
            {measurement}
          </Typography>
        </Box>
      </Box>
    </Paper>
  );
};

export default MetricCardInfo;
