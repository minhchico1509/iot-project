import * as React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import {
  DateTimePicker as MUIDateTimePicker,
  DateTimePickerProps,
} from "@mui/x-date-pickers/DateTimePicker";
import { Dayjs } from "dayjs";

type TDateTimePickerProps = DateTimePickerProps<Dayjs> & {
  errorMessage?: string;
};

const DateTimePicker: React.FC<TDateTimePickerProps> = ({
  sx,
  errorMessage,
  ...otherProps
}) => (
  <LocalizationProvider dateAdapter={AdapterDayjs}>
    <MUIDateTimePicker
      {...otherProps}
      slotProps={{
        textField: {
          error: !!errorMessage,
          helperText: errorMessage,
          size: "small",
          sx: {
            "& .MuiFormHelperText-root": { marginLeft: "3px" },
            ...sx,
          },
        },
      }}
    />
  </LocalizationProvider>
);

export default DateTimePicker;
