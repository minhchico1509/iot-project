import React from "react";
import { MenuItem, TextField, TextFieldProps, styled } from "@mui/material";

interface IDropdownOption {
  label: string;
  value: string;
}
type TDropdownProps = TextFieldProps & {
  options: IDropdownOption[];
};

const StyledTextField = styled(TextField)({
  "& .MuiFormHelperText-root": {
    marginLeft: "3px",
  },
});

const Dropdown: React.FC<TDropdownProps> = ({ options, ...otherProps }) => {
  return (
    <StyledTextField size="small" select {...otherProps}>
      {options.map((option, index) => (
        <MenuItem key={index} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </StyledTextField>
  );
};

export default Dropdown;
