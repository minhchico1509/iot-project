import { Backdrop, CircularProgress } from "@mui/material";

export const Loading = () => {
  return (
    <Backdrop
      open={true}
      sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.tooltip + 1 }}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};
