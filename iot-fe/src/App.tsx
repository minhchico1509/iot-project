import { StyledEngineProvider } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import AppRoutes from "./routes/AppRoutes";
import SocketProvider from "./contexts/SocketContext";

function App() {
  const queryClient = new QueryClient();
  return (
    <StyledEngineProvider>
      <BrowserRouter>
        <QueryClientProvider client={queryClient}>
          <SocketProvider>
            <AppRoutes />
          </SocketProvider>
        </QueryClientProvider>
      </BrowserRouter>
    </StyledEngineProvider>
  );
}

export default App;
