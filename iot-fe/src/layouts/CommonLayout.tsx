import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";

import Header from "./Header";

const CommonLayout = () => {
  return (
    <>
      <Header />
      <Box p={4}>
        <Outlet />
      </Box>
    </>
  );
};

export default CommonLayout;
