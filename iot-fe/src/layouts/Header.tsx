import { AppBar, Box, Toolbar, Container, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

import { APP_BAR_LIST } from "src/constants";

const Header = () => {
  const navigate = useNavigate();

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {APP_BAR_LIST.map((item, index) => (
              <Button
                key={index}
                onClick={() => navigate(item.path)}
                sx={{ my: 2, color: "white", display: "block" }}
              >
                {item.label}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Header;
