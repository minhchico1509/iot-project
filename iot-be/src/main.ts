import * as mqtt from "mqtt";
import express, { Application } from "express";
import cors from "cors";
import http from "http";
import { Server } from "socket.io";
import sequelize from "./configs/sequelize.config";
import MetricModel from "./models/metric.model";
import { Op } from "sequelize";

const app: Application = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = http.createServer(app);
const io = new Server(server, {
  cors: { origin: "*" },
});
const mqttClient = mqtt.connect("mqtt://broker.hivemq.com:1883");

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Connect to database successfully");
  } catch (error) {
    console.log("Failed to connect to database");
  }
})();

app.get("/metrics", async (req, res) => {
  try {
    const { startDateTime, endDateTime, quality, location } = req.query;
    const conditions: any = {};
    conditions["createdAt"] = {
      [Op.gte]: new Date((startDateTime as string) || 0),
      [Op.lte]: endDateTime ? new Date(endDateTime as string) : new Date(),
    };
    quality && (conditions["airQuality"] = quality);
    location && (conditions["location"] = location);
    const metrics = await MetricModel.findAll({
      where: conditions,
      order: [["createdAt", "ASC"]],
    });
    res.status(200).json(metrics);
  } catch (error) {
    res.status(500).json(error);
  }
});

io.on("connection", async (socketClient) => {
  console.log("Client Connected: ", socketClient.id);

  socketClient.on("disconnect", () => {
    console.log("A user disconnected");
  });
  //Server publish topic "esp32/fan" lên Hivqmq broker
  // Change this part of the code
  socketClient.on("toggle-fan", async ({ location, status }) => {
    await mqttClient?.publishAsync(
      "esp32/controlfan",
      JSON.stringify({ location, status })
    );
  });
});

mqttClient?.on("connect", async () => {
  console.log("MQTT Connected");
  await mqttClient?.subscribeAsync("esp32/metrics");
  await mqttClient?.subscribeAsync("esp32/fan");
  await mqttClient?.subscribeAsync("esp32/controlfan");
});

mqttClient?.on("message", async (topic, message) => {
  try {
    console.log(
      "[MQTT Received] Topic:",
      topic,
      ", Message:",
      message.toString()
    );
    const data = JSON.parse(message.toString());

    if (topic === "esp32/metrics") {
      for (const item of data) {
        //Thực hiên lưu dữ liệu vào trong database
        const newMetricInserted = await MetricModel.create({
          ...item,
          airQuality: item.airQualityIndex > 45 ? "POLLUTION" : "FRESH",
        });
      }
    }
    //Emit dữ liệu cho client
    io.emit(topic, data);
  } catch (error) {
    console.log(error);
  }
});

void sequelize.sync();

server.listen(8080, () => {
  console.log(`Server is running on port 8080`);
});
