import {
  Model,
  DataTypes,
  CreationOptional,
  InferAttributes,
  InferCreationAttributes,
} from "sequelize";
import sequelize from "src/configs/sequelize.config";

interface IMetric
  extends Model<InferAttributes<IMetric>, InferCreationAttributes<IMetric>> {
  id: CreationOptional<string>;
  location: string;
  temperature: number;
  humidity: number;
  airQualityIndex: number;
  airQuality: string;
}

const MetricModel = sequelize.define<IMetric>(
  "Metric",
  {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      unique: true,
    },
    location: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    temperature: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    humidity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    airQualityIndex: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    airQuality: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    tableName: "metric",
    timestamps: true,
  }
);

export type { IMetric };
export default MetricModel;
