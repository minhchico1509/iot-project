import { Sequelize } from "sequelize";

const sequelize = new Sequelize({
  database: "iot-project",
  username: "root",
  password: "your_password",
  host: "localhost",
  port: 3306,
  dialect: "mysql",
});

export default sequelize;
